#include "funzioni.h"

/*
int leggi(char*, struct*)
2 parametri:
	1) nome del file da leggere
	2) struct vuota

Apre un canale di comunicazione col file "vestiti.txt" in modalita' lettura,
poi memorizza tutti i dati del file nella struct passata come parametro

return: int
	0 = ERR
	1 = OK
*/
int leggi(char* nomefile, Vestiti* v){
	FILE* fp = fopen(nomefile, "r");

	if(!fp){
        return 0; // ERROR
	}

    (*v).count = 0; // prima di riempire, inizializzo il contatore a 0
    while(!feof(fp)){
        fscanf(fp,"%d %d %d %f",&(*v).vestito[(*v).count].ID,&(*v).vestito[(*v).count].TG,&(*v).vestito[(*v).count].QNT,&(*v).vestito[(*v).count].PRZ);
        (*v).count += (*v).vestito[(*v).count].QNT; // aggiorno la quantita di vestiti
    }

	fclose(fp);

	return 1; // OK
}

/*
void estrai(int, struct, struct*)
3 parametri:
	1) taglia scelta
	2) struct con vestiti gia letti da file (v_letti)
	3) struct vuota (v_taglia) passata per riferimento.

I vestiti della struct v_letti che hanno la taglia passata come parametro
vengono memorizzati nella struct v_taglia
*/
void estrai(int TG, Vestiti v_letti, Vestiti* v_taglia){
	int i;
	(*v_taglia).count = 0;

	for (i = 0; i < v_letti.count; ++i)
	{
		if(v_letti.vestito[i].TG == TG){
			(*v_taglia).vestito[(*v_taglia).count].ID = v_letti.vestito[i].ID;
			(*v_taglia).vestito[(*v_taglia).count].TG = v_letti.vestito[i].TG;
			(*v_taglia).vestito[(*v_taglia).count].QNT = v_letti.vestito[i].QNT;
			(*v_taglia).vestito[(*v_taglia).count].PRZ = v_letti.vestito[i].PRZ;
			(*v_taglia).count++;
		}
	}
}

/*
* La funzione quanti() restituisce il numero di 
* vestiti presenti nella struct passata come parametro
*/
int quanti(Vestiti v){
	return v.count;
}

/*
* La funzione prezzo() restituisce il prezzo totale di tutti
* i vestiti presenti nella struct passata come parametro
*/
float prezzo(Vestiti v){
	int i;
	float somma = 0;

	for (i = 0; i < v.count; ++i) somma += v.vestito[i].PRZ;

	return somma;
}

/*
int scrivi(char*, struct*)
2 parametri:
	1) nome del nuovo file
	2) struct con vestiti letti da file

Per ogni vestito presente nella struct con taglia 
compresa tra 42 e 54 scrive sul nuovo file una riga con: 
Taglia - Quantita di vestiti di quella taglia - Prezzo totale di quei vestiti

return: int
	0 = ERR
	1 = OK
*/
int scrivi(char* nomefile_tot, Vestiti* letti){
	int i,j = 0;
	int doppi[MAX_DIM]; // array contenente le taglie gia scritte, in modo da non scriverle due volte
	Vestiti estratti;

	FILE* fp = fopen(nomefile_tot, "w");

	if(!fp){
        return 0; //ERROR
	}

	for (i = 0; i < (*letti).count; ++i)
	{
		if( (*letti).vestito[i].TG >= 42 && (*letti).vestito[i].TG <= 54 ){

            if(trova_doppione( (*letti).vestito[i].TG , doppi) == 0){ // 0 = NON ha trovato un doppione

                doppi[j] = (*letti).vestito[i].TG; // salvo le taglie gia controllate in modo da non scrivere due righe uguali
                j++;

                estrai((*letti).vestito[i].TG , (*letti), &estratti);

                fprintf(fp,"\n%d %d %f", (*letti).vestito[i].TG, quanti(estratti), prezzo(estratti) );
            }
		}
	}

	fclose(fp);

	return 1; // andato a buon fine
}

/*
int trova_doppione(int,int [])
2 parametri: 
	1) la taglia da controllare 
	2) l'array contenente le taglie gia scritte su file

controlla l'array per vedere se la taglia passata
come parametro e' gia' stata scritta sul file

return: int
	0 = doppione non trovato
	1 = doppione trovato
*/
int trova_doppione(int TG, int doppi[]){
    int i;
    for(i = 0; i < 20; i++) if(TG == doppi[i]) return 1; //DOPPIONE TROVATO
    return 0; //DOPPIONE NON TROVATO
}