#include <stdio.h>
#include <stdlib.h>

#define MAX_DIM 20
#define NOMEFILE "vestiti.txt"
#define NOMEFILE_TOT "totali.txt"

typedef struct v1{
	int ID,TG,QNT;
	float PRZ;
}Vestito;

typedef struct v2{
	Vestito vestito[MAX_DIM];
	int count; //numero di vestiti letti da file
}Vestiti;

int leggi(char* nomefile, Vestiti* v);

void estrai(int TG, Vestiti v_letti, Vestiti* v_taglia);

int quanti(Vestiti v);

float prezzo(Vestiti v);

int scrivi(char* nomefile_tot, Vestiti* letti);

int trova_doppione(int TG, int doppi[]);
